﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FileRecordConnectionDrop : MonoBehaviour
{
    public string m_folderFail = "Fail";
    public string m_folderBitOfLag = "SucceedBut_1sec";
    public string m_folderHeavyLag = "SucceedBut_3sec";
    public string m_folderWowLag = "SucceedBut_5sec";
    public void SaveAlmostDisconnection(ConnectionOnOff onOff)
    {
      
        if (onOff.m_succedToRecover == true && onOff.GetTimeToRecover() > 5.0)
            SaveCheckIn(m_folderWowLag, onOff);
        else if (onOff.m_succedToRecover == true && onOff.GetTimeToRecover() > 3.0)
            SaveCheckIn(m_folderHeavyLag, onOff); 
        else if (onOff.m_succedToRecover == true && onOff.GetTimeToRecover() > 1.0)
            SaveCheckIn(m_folderBitOfLag, onOff);

    }
    public void SaveDisconnection(DisconnectionInfo recoverdConnection)
    {
   
        UnityDirectoryStorage.SaveFile("ConnectionDrop/" + m_folderFail, recoverdConnection.m_lostConnection.ToString("yyyy_MM_dd_hh_mm_ss_fff") + ".txt",
            "Duration:"+recoverdConnection.GetDisconnectionLenght(), true);

    }
    private void SaveCheckIn(string folderName, ConnectionOnOff onOff)
    {
        UnityDirectoryStorage.SaveFile("ConnectionDrop/" + folderName, onOff.m_startRecoveringInfo.ToString("yyyy_MM_dd_hh_mm_ss_fff") + ".txt",
            "Duration:" + onOff.GetTimeToRecover(), true);
    }

    public void OpenDirectory()
    {
        string path = UnityDirectoryStorage.GetPersistantThroughResintallFolder() + "/ConnectionDrop";
        if(Directory.Exists(path))
            Application.OpenURL(path);
    }
    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }
}
