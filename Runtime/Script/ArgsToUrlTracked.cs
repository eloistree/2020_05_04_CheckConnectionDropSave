﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class ArgsToUrlTracked : MonoBehaviour
{
    public CheckConnection m_connectionChecker;
    void Awake()
    {
        string[] args = System.Environment.GetCommandLineArgs();
        string url = "";
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i].StartsWith("URL_"))
            {
                url = args[i].Substring(4);
            }

        }
        if (url.Length > 0)
        { 
            m_connectionChecker.m_url = url;
            StartCoroutine(SaveAsImage(url));
           
        }



    }

    private IEnumerator SaveAsImage(string url)
    {
        WWW img = new WWW(url);
        yield return img;
        if (img.error == null)
            SaveTextureAsPNG(img.texture, UnityDirectoryStorage.GetPersistantThroughResintallFolder()+"/UsedImage.png");
    }

    public static void SaveTextureAsPNG(Texture2D _texture, string _fullPath)
    {
        byte[] _bytes = _texture.EncodeToPNG();
        System.IO.File.WriteAllBytes(_fullPath, _bytes);
        Debug.Log(_bytes.Length / 1024 + "Kb was saved as: " + _fullPath);
    }

}
